module gitlab.com/smueller18/helm-plugin-dependency-update

go 1.13

require (
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/tools v0.0.0-20200606014950-c42cb6316fb6 // indirect
	helm.sh/helm/v3 v3.1.2
)
