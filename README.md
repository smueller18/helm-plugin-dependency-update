# helm-plugin-dependency-update

Helm plugin for updating chart dependencies to latest versions.

## Installation

```bash
go build .
helm plugin install .
```

## Usage

```bash
cd $HELM_CHART
helm dependency-update
```
