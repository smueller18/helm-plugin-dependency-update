package main

import (
	"fmt"
	"helm.sh/helm/v3/pkg/action"
	"helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/chart/loader"
	"helm.sh/helm/v3/pkg/chartutil"
	"helm.sh/helm/v3/pkg/cli"
	"helm.sh/helm/v3/pkg/helmpath"
	"helm.sh/helm/v3/pkg/repo"
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

func main() {

	path, err := os.Getwd()
	if err != nil {
    		log.Println(err)
	}

	c, err := loader.Load(path)
	if err != nil {
		fmt.Println(err)
	}
	if c.Metadata.Dependencies == nil {
		log.Print("Found no dependencies, stopping.")
		os.Exit(0)
	}

	if ensureRepos(c) {
		repoUpdate()
	}
	res := fetchRepositories()

	for i := range c.Metadata.Dependencies {
		dependency := c.Metadata.Dependencies[i]

		upstreamVersion := res[dependency.Repository][dependency.Name][0].Version

		if dependency.Version != upstreamVersion {
			fmt.Printf("Update dependency %s from version %s to %s.\n", dependency.Name, dependency.Version, upstreamVersion)
			dependency.Version = upstreamVersion

			dependencyPath := filepath.Join(path, "charts", dependency.Name)
			if _, err := os.Stat(dependencyPath); !os.IsNotExist(err) {
				fmt.Printf("Remove folder %s\n", dependencyPath)
				if err := os.RemoveAll(dependencyPath); err != nil {
					log.Fatal(err)
				}
			}

			pull := action.NewPull()
			pull.Untar = true
			pull.UntarDir = filepath.Join(path, "charts")
			pull.RepoURL = dependency.Repository
			pull.Version = dependency.Version
			pull.Settings = cli.New()

			fmt.Printf("Pull chart %s/%s and extract to %s/%s\n", dependency.Repository, dependency.Name, pull.UntarDir, dependency.Name)
			_, err := pull.Run(dependency.Name)
			if err != nil {
				log.Fatal(err)
			}

			files, err := filepath.Glob(filepath.Join(pull.UntarDir, "*.tgz"))
			if err != nil {
				log.Fatal(err)
			}
			for _, fname := range files {
				fmt.Printf("Removing file %s\n", fname)
				if err := os.Remove(fname); err != nil {
					log.Fatal(err)
				}
			}

		} else {
			fmt.Printf("Dependency %s is already up to date.\n", dependency.Name)
		}
	}

	if err = chartutil.SaveChartfile(path+"/Chart.yaml", c.Metadata); err != nil {
		log.Fatal(err)
	}

}

func fetchRepositories() map[string]map[string]repo.ChartVersions {
	cliConfig := cli.New()
	rf, err := repo.LoadFile(cliConfig.RepositoryConfig)
	if err != nil || len(rf.Repositories) == 0 {
		log.Fatal("no repositories configured")
	}

	repos := make(map[string]map[string]repo.ChartVersions)

	for _, re := range rf.Repositories {
		file := filepath.Join(cliConfig.RepositoryCache, helmpath.CacheIndexFile(re.Name))
		index, err := repo.LoadIndexFile(file)
		if err != nil {
			fmt.Printf("WARNING: Repo %q is corrupt or missing. Try 'helm repo update'.", re.Name)
			continue
		}
		repos[re.URL] = index.Entries

	}

	return repos
}

func ensureRepos(chart *chart.Chart) bool {
	f, err := repo.LoadFile(cli.New().RepositoryConfig)
	if err != nil {
		log.Fatal(err)
	}

	updated := false
	for i := range chart.Metadata.Dependencies {
		dependency := chart.Metadata.Dependencies[i]
		foundRepo := false
		for j := range f.Repositories {
			if dependency.Repository == f.Repositories[j].URL {
				foundRepo = true
			}
		}
		if !foundRepo {
			fmt.Printf("URL %s not found in repositories. Adding repository %s.\n", dependency.Repository, dependency.Repository)
			repoAdd(dependency.Repository)
			updated = true
		}
	}
	return updated
}

func repoAdd(repo string) {
	runCmd("helm", "repo", "add", repo, repo)
}

func repoUpdate() {
	runCmd("helm", "repo", "update")
}

func runCmd(name string, args ...string) {
	cmd := exec.Command(name, args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	fmt.Println("$", cmd.String())

	if err := cmd.Run(); err != nil {
		log.Fatal("Stopping because of error")
	}
}
